# Generated by Django 5.0.1 on 2024-02-12 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personal_blog', '0003_alter_post_author_alter_post_body_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='name',
            field=models.CharField(max_length=200, verbose_name='Author name'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='author',
            field=models.CharField(blank=True, default='Anonymous', max_length=50, null=True, verbose_name='Author'),
        ),
        migrations.AlterField(
            model_name='comment',
            name='text',
            field=models.TextField(max_length=1000, verbose_name='Comment'),
        ),
    ]
