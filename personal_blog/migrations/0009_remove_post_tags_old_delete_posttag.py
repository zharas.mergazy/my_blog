# Generated by Django 5.0.1 on 2024-02-28 07:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('personal_blog', '0008_auto_20240228_0647'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='tags_old',
        ),
        migrations.DeleteModel(
            name='PostTag',
        ),
    ]
