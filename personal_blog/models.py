from django.db import models
from .helpers.validators import at_least_3


class Author(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name = 'Author name')

    def __str__(self) -> str:
        return self.name
    
class Post(models.Model):
    title = models.CharField(max_length = 200, null=False, blank=False, verbose_name = 'Title')
    body = models.TextField(max_length = 3000, null=False, blank=False, verbose_name='Text', validators=(at_least_3,))
    author = models.ForeignKey(to = Author, on_delete=models.CASCADE, null=False, verbose_name='Author')
    tags = models.ManyToManyField(
        'personal_blog.Tag',
        related_name='posts',
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name = 'Creation date and time')
    updated_at = models.DateTimeField(auto_now_add=True, verbose_name = 'Update date and time')

    def __str__(self):
        return f"{self.pk} - {self.title}"
    
class Comment(models.Model):
    text = models.TextField(max_length = 1000, null=False, blank=False, verbose_name='Comment')
    author = models.CharField(max_length = 50, null=True, blank=True, default="Anonymous", verbose_name='Author')
    post = models.ForeignKey('personal_blog.Post', on_delete=models.CASCADE, related_name='comments', verbose_name='Post')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now_add=True, verbose_name='Update date and time')

    def __str__(self) -> str:
        return self.text[:20]
    
class Tag(models.Model):
    name = models.CharField(max_length=30, verbose_name='Tag')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = 'Creation date and time')

    def __str__(self) -> str:
        return self.name
    

