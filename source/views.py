from django.views.generic import View, TemplateView
from django.shortcuts import render


class IndexView(TemplateView):
    template_name = 'index.html'
    